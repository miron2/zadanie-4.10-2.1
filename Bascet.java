public class Bascet {


    private String items = "";
    private int totlePris = 0;
    private int limit;
    private double totleWeight = 0;
    private double weight = 0;
    private int count = 0;


    public Bascet() {
        items = "Список товаров:";
        this.limit = 1000000;

    }

    public Bascet(int limit) {
        this();
        this.limit = limit;
    }

    public Bascet(String items, int totlePris, double totleWeight) {
        this();
        this.items = items ;
        this.totlePris = totlePris;
        this.totleWeight = totleWeight;
    }

    public void add(String name, int price) {
        add(name, price, count, weight);

    }

    public void add(String name, int price, int count, double weight) {
        if (items.contains(name)) {
            return;
        }
        if (totlePris + price >= limit) {
            return;
        }
        items = items + "\n" + name + "-" + count + "шт.-" + price + " руб." +
                "-" + weight + "Кг";
        totlePris = totlePris + count * price;
        totleWeight = totleWeight + count * weight;
    }


    public void clear() {
        items = "";
        totlePris = 0;
        totleWeight = 0;
    }

    public int getTotalPrice() {
        return totlePris;
    }

    public double getTotalWeight() {
        return totleWeight;
    }

    public boolean contains(String name) {
        return items.contains(name);
    }

    public void print(String title) {
        System.out.println(title);
        if (items.isEmpty()) {
            System.out.println("Корзина пуста");
        } else {
            System.out.println(items);
            System.out.println("Соимость корзины"+ "-" + totlePris + " руб.");
            System.out.println("Вес корзины - " + totleWeight + " Кг" );
        }

    }
}


